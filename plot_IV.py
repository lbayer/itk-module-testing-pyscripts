import sys
import json
import argparse
import numpy as np
import scipy.optimize as so
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.lines import Line2D

#===================================================================================================
# IV plotter for ITk module testing
# version:  1.0
# author:   Lukas Bayer
#===================================================================================================

class IV_plotter:

    def __init__(self, inputs, output):
        self.multicurves = len(inputs) > 1
        self.inputs = inputs
        self.output = output
        self.datadicts = []
        self.read_data()
        self.plot_IVs()

    def read_data(self):
        for filename in self.inputs:
            if filename[-5:] != ".json" : sys.exit(f"Error: {filename} is not a .json file")
            json_file = open(str(filename))
            jdata = json.load(json_file)
            currentAMAC =  np.array(jdata["results"]["CURRENT"])
            currentPS   =  np.array(jdata["results"]["PS_CURRENT"])
            voltage     =  np.array(jdata["results"]["VOLTAGE"])
            info        = "passed" if jdata["passed"] else "failed"
            label       = jdata["component"]
            newdict = {
                "currentAMAC": currentAMAC,
                "currentPS"  : currentPS,
                "voltage"    : voltage,
                "info"       : info,
                "label"      : label,
            }
            self.datadicts.append(newdict)
        
    def plot_IVs(self):

        # make figure
        plt.figure(figsize=(4.5,3))

        # get color list
        prop_cycle = plt.rcParams['axes.prop_cycle']
        colors = prop_cycle.by_key()['color']

        # make axes
        plt.xlabel(r"$U$ [V]")
        ax1 = plt.gca()
        ax2 = ax1.twinx()
        if self.multicurves:
            color1 = "black"
            color2 = "black"
        else:
            color1 = colors[0]
            color2 = colors[1]
        ax1.spines['left'].set_color(color1) 
        ax2.spines['left'].set_color(color1) 
        ax1.spines['right'].set_color(color2) 
        ax2.spines['right'].set_color(color2)
        ax1.set_ylabel(r"$I_{AMAC}$ [nA]", color=color1)
        ax2.set_ylabel(r"$I_{PS}$ [uA]"  , color=color2)
        ax1.ticklabel_format(axis='y', style='sci', scilimits=(0,4))
        ax2.ticklabel_format(axis='y', style='sci', scilimits=(0,4))
        ax1.tick_params(axis='y', colors=color1)
        ax2.tick_params(axis='y', colors=color2)

        # plot data
        for i, dd in enumerate(self.datadicts):
            c1 = colors[i] if self.multicurves else colors[0]
            c2 = colors[i] if self.multicurves else colors[1]
            ax1.plot(dd['voltage'], dd['currentAMAC'], "-" , color=c1, label=dd["label"])
            ax2.plot(dd['voltage'], dd['currentPS']  , "--", color=c2)

        # add legend
        h1, l1 = ax1.get_legend_handles_labels()
        h2, l2 = ax2.get_legend_handles_labels()
        h = [Line2D([0], [0], color=color1, lw=1, linestyle="-" , label='AMAC current'),
             Line2D([0], [0], color=color2, lw=1, linestyle="--", label='PS current')]
        l=['AMAC current', 'PS current']
        ax2.legend(h, l, loc='lower center', fontsize=6)
        if self.multicurves:
            ax1.legend(h1+h2, l1+l2, loc='upper center', fontsize=6)
        else:
            infotext = f"{self.datadicts[0]['label']}: {self.datadicts[0]['info']}"
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
            plt.text(0.5, 0.95, infotext, transform=ax1.transAxes, fontsize=6, verticalalignment='center', horizontalalignment='center', bbox=props)

        # add grid (for which axis?)
        ax1.grid()
        #ax2.grid()

        # save
        plt.tight_layout()
        plt.savefig(f"{self.output}")
        plt.close()


#===================================================================================================

if __name__ == '__main__':

    # parse arguments
    parser = argparse.ArgumentParser(description="Plotting module IV curves from N input json files")
    parser.add_argument('-i', '--input', action='store', type=str, nargs='*', default='input.json', required=True, help='List of input json files separated by a space')
    parser.add_argument('-o', '--output', help='Name of the output directory', default = './', type = str)
    parser.add_argument('-n', '--name', help='Name of the plot', default = 'IV', type = str)
    args = parser.parse_args()
    output_dir  = args.output if args.output[-1] == '/' else args.output + '/'
    output_file = f"{output_dir}{args.name}.pdf"

    # print names of input and output files
    print(f" Input: {args.input[0]}")
    if len(args.input) > 1:
        for filename in args.input[1:]:
            print(f"        {filename}")
    print(f"Output: {output_file}")

    ivp = IV_plotter(args.input, output_file)