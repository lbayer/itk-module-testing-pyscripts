import sys
import json
import argparse
import numpy as np
import scipy.optimize as so
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.lines import Line2D

#===================================================================================================
# RC plotter for ITk module testing
# version:  1.2
# author:   Lukas Bayer
# TO DO:    check that charge values are correct!
#===================================================================================================

class RC_plotter:

    def __init__(self, inputs, output):
        self.multicurves = len(inputs) > 1
        self.inputs = inputs
        self.output = output
        self.datadicts = []
        #self.tp_charges = [0.5, 1.0, 1.5]                                       # three point gain
        #self.rc_charges = [0.2, 0.5, 0.8, 1.0, 1.25, 1.5, 2.0, 3.0, 4.0, 6.0]   # response curve
        self.chips   = [0, 1, 2, 3, 4, 5]
        self.streams = [0,1]
        self.labels  = None
        self.read_data()

    def run(self):
        #self.plot_overview()
        self.plot_cnoise()

    def set_stream(self, stream):
        if stream == 0 or stream == 1:
            self.streams = [stream]

    def set_labels(self, labels):
        if len(labels) != len(self.inputs):
            print(f"WARNING: number of labels ({len(labels)}) doesn't match number of input files ({len(self.inputs)}) --> ignore labels")
            return
        self.labels = labels

    def read_data(self):
        for filename in self.inputs:
            if filename[-5:] != ".json" : sys.exit(f"Error: {filename} is not a .json file")
            json_file = open(str(filename))
            jdata = json.load(json_file)
            #for key in jdata:
            #    print(f"key: {key}")
            info        = "passed" if jdata["passed"] else "failed"
            name        = jdata["properties"]["det_info"]["name"]
            m_vt50_away   =  np.array(jdata["results"]["vt50_mean_away"])
            m_vt50_under  =  np.array(jdata["results"]["vt50_mean_under"])
            m_gain_away   =  np.array(jdata["results"]["gain_mean_away"])
            m_gain_under  =  np.array(jdata["results"]["gain_mean_under"])
            m_outnoise_away  =  np.array(jdata["results"]["outnse_away"])
            m_outnoise_under =  np.array(jdata["results"]["outnse_under"])
            m_innoise_away   =  np.array(jdata["results"]["innse_mean_away"])
            m_innoise_under  =  np.array(jdata["results"]["innse_mean_under"])
            c_innoise_away   =  np.hstack(jdata["results"]["innse_away"])
            c_innoise_under  =  np.hstack(jdata["results"]["innse_under"])
            #c_innoise_away  = jdata["results"]["innse_away"]
            #c_innoise_under = jdata["results"]["innse_under"]
            #c_innoise_away  = np.concatenate([np.array(i) for i in c_innoise_away])
            #c_innoise_under = np.concatenate([np.array(i) for i in c_innoise_under])
            newdict = {
                "info": info,
                "name": name,
                "m_vt50": [m_vt50_under,m_vt50_away],
                "m_gain": [m_gain_under,m_gain_away],
                "m_outnoise": [m_outnoise_under,m_outnoise_away],
                "m_innoise": [m_innoise_under,m_innoise_away],
                "c_innoise": [c_innoise_under,c_innoise_away],
            }
            self.datadicts.append(newdict)

    #===============================================================================================
        
    def plot_overview(self):
        print("plotting overview...")

        # make figure with 4 subplots
        fig, axs = plt.subplots(2,2,figsize=(9, 6))

        # get color list
        #colors = list(mcolors.CSS4_COLORS.values())
        prop_cycle = plt.rcParams['axes.prop_cycle']
        colors = prop_cycle.by_key()['color']

        # VT50
        axs[0,0].grid()
        axs[0,0].set_xlabel("chip")
        axs[0,0].set_ylabel("mean VT50 [mV]")

        # gain
        axs[0,1].grid()
        axs[0,1].set_xlabel("chip")
        axs[0,1].set_ylabel("mean gain [mV/fC]")

        # output noise
        axs[1,0].grid()
        axs[1,0].set_xlabel("chip")
        axs[1,0].set_ylabel("mean output noise [mV]")

        # input noise
        axs[1,1].grid()
        axs[1,1].set_xlabel("chip")
        axs[1,1].set_ylabel("mean input noise ENC [fC]")

        # plot data
        for idata, dd in enumerate(self.datadicts):
            for istream in range(2):
                label = f"{dd['name']} S{istream}"
                #print(label)
                
                axs[0,0].plot(self.chips, dd["m_vt50"][istream], "x" , color=colors[2*idata+istream], label=label)
                axs[0,1].plot(self.chips, dd["m_gain"][istream], "x" , color=colors[2*idata+istream], label=label)
                axs[1,0].plot(self.chips, dd["m_outnoise"][istream], "x" , color=colors[2*idata+istream], label=label)
                axs[1,1].plot(self.chips, dd["m_innoise"][istream], "x" , color=colors[2*idata+istream], label=label)

        axs[0,0].legend(loc='best', fontsize=6)
        axs[0,1].legend(loc='best', fontsize=6)
        axs[1,0].legend(loc='best', fontsize=6)
        axs[1,1].legend(loc='best', fontsize=6)

        axs[0,0].set_ylim((0,500))
        axs[0,1].set_ylim((0,140))
        axs[1,0].set_ylim((0,30))
        axs[1,1].set_ylim((0,1450))

        # save
        plt.tight_layout()
        plt.savefig(f"{self.output}_overview.pdf")
        plt.close()
        return

    #===============================================================================================

    def plot_cnoise(self):
        print("plotting channel noise...")

        # make figure
        fig, ax = plt.subplots(figsize=(9, 6))

        # get color list
        #colors = list(mcolors.CSS4_COLORS.values())
        prop_cycle = plt.rcParams['axes.prop_cycle']
        colors = prop_cycle.by_key()['color']

        # input noise
        ax.grid()
        ax.set_xlabel("channel")
        ax.set_ylabel("input noise ENC [fC]")

        # plot data
        for idata, dd in enumerate(self.datadicts):
            for istream in self.streams:
                if self.labels == None:
                    label = f"{dd['name']} S{istream}"
                else:
                    label = f"{dd['name']} S{istream} {self.labels[idata]}"
                channels = np.arange(0,dd["c_innoise"][istream].size,1)
                ax.plot(channels, dd["c_innoise"][istream], "." , label=label)#, color=colors[2*idata+istream])

        ax.legend(loc='best', fontsize=6)
        ax.set_ylim((0,2400))

        # save
        plt.tight_layout()
        plt.savefig(f"{self.output}_cnoise.pdf")
        plt.savefig(f"{self.output}_cnoise.png")
        plt.close()
        return

#===================================================================================================

if __name__ == '__main__':

    # parse arguments
    parser = argparse.ArgumentParser(description="Plotting module response curves from N input json files")
    parser.add_argument('-i', '--input', action='store', type=str, nargs='*', default='input.json', required=True, help='List of input json files separated by a space')
    parser.add_argument('-o', '--output', help='Name of the output directory', default = './', type = str)
    parser.add_argument('-n', '--name', help='Name of the plot', default = 'RC', type = str)
    parser.add_argument('-s', '--stream', help='Stream (-1=both, 0=under, 1=away)', default = -1, type = int)
    parser.add_argument('-l', '--labels', action='store', type=str, nargs='*', default='default', help='List of labels, one for each input file')
    args = parser.parse_args()
    output_dir  = args.output if args.output[-1] == '/' else args.output + '/'
    output_file = f"{output_dir}{args.name}"

    # print names of input and output files
    print(f' Input: {args.input[0]} {f"({args.labels[0]})" if args.labels != "default" else ""}')
    if len(args.input) > 1:
        for i,filename in enumerate(args.input[1:]):
            print(f'        {filename} {f"({args.labels[i+1]})" if args.labels != "default" else ""}')
    print(f"Output: {output_file}")

    rcp = RC_plotter(args.input, output_file)
    rcp.set_stream(args.stream)
    if args.labels != 'default':
        rcp.set_labels(args.labels)
    rcp.run()

    print("done")