# Plotting scripts for ITk module testing with Grey Box and Cold Box
==================================================================

## General:
- please use python3 to run scripts
- inputs have to be the .json files from respective ITSDAQ tests

==================================================================

## plot_IV.py

this script plots two IV curves:
1. power supply output current vs voltage
2. current measured by AMAC vs supplied voltage

Use the following command:
python3 plot_IV.py -i <input_file(s).json> -o <output_directory> -n <name_of_output>

You have to provide a single input file or multiple input files separated by spaces.
The output directory is optional, default is current working directory. But it has to exist prior to running the script.
Name of output is also optional, default is "IV". Output files are created as pdf and the ending ".pdf" is appended automatically.

==================================================================

## plot_RC.py

same as plot_IV.py but for response curve / channel noise